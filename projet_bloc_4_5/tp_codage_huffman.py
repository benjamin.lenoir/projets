from binary_tree import BinaryTree as BT

import graphviz
import binary_IO
from IPython.display import display
def show_tree(arbre):
    display(graphviz.Source(arbre.to_dot()))

texte_source = '''La Cigale et la Fourmi

La Cigale, ayant chanté
Tout l'été,
Se trouva fort dépourvue
Quand la bise fut venue :
Pas un seul petit morceau
De mouche ou de vermisseau.
Elle alla crier famine
Chez la Fourmi sa voisine,
La priant de lui prêter
Quelque grain pour subsister
Jusqu'à la saison nouvelle.
Je vous paierai, lui dit-elle,
Avant l'Oût, foi d'animal,
Intérêt et principal. 
La Fourmi n'est pas prêteuse :
C'est là son moindre défaut.
Que faisiez-vous au temps chaud ?
Dit-elle à cette emprunteuse.
Nuit et jour à tout venant
Je chantais, ne vous déplaise.
Vous chantiez ? j'en suis fort aise.
Eh bien! dansez maintenant.
''' 

def sequence_to_occurrences(seq):
    '''
    :param seq: une séquence de symboles 
    :return: (dict) un dictionnaire donnant le nombre 
          d'occurrences de chacun des symboles contenu dans seq
    :CU: les symboles de seq doivent pouvoir être une clé d'un dictionnaire.
    :Exemple:
    
    >>> sequence_to_occurrences('Codage de Huffman') == {'C': 1, 'o': 1, 'd': 2, 'a': 2, 'g': 1, 'e': 2, ' ': 2, 'H': 1, 'u': 1, 'f': 2, 'm': 1, 'n': 1}
    True
    '''
    dico={}
    for car in seq:
        if car in dico:
            dico[car]+=1            
        else:
            dico.update({car:1})
    return dico

occ_source = sequence_to_occurrences(texte_source)


exple_occ = {'a':5, 'b':2, 'c':1, 'd':1}


def weight(tree):
    '''
    :param tree: (BinaryTree) un arbre binaire étiqueté par des couples (truc, nombre)
    :return: (int ou float) le poids de l'arbre, (ie le nombre composante droite du couple étiquette de l'arbre)
     '''
    if not tree.is_empty():
        return tree.get_data()[1]


VIDE = BT()
foret = [BT(('a', exple_occ['a']), VIDE, VIDE), BT(('b', exple_occ['b']), VIDE, VIDE),
         BT(('c', exple_occ['c']), VIDE, VIDE), BT(('d', exple_occ['d']), VIDE, VIDE)]


def insert(tree, forest):
    '''
    :param tree: (BinaryTree) un arbre étiqueté par des couples (truc, nombre)
    :param forest: (list) une liste d'arbres binaires étiquetés par des couples (truc, nombre) rangés dans l'ordre décroissant des poids
    :return: (NoneType)
    :side effect: modifie la foret en insérant l'arbre à la première position qui maintient la forêt rangée en ordre décroissant
    '''
    if tree.is_empty() or forest == [] or weight(tree) <= weight(forest[-1]):
        forest.append(tree)
    elif weight(tree) > weight(forest[0]):
        forest.insert(0,tree)
    else:
        ind = 1
        while weight(tree) < weight(forest[ind]):
            ind += 1
        forest.insert(ind,tree)


arb1 = foret.pop()
arb2 = foret.pop()
arb3 = BT(('', weight(arb1) + weight(arb2)), arb1, arb2)
insert(arb3, foret)


arb1 = foret.pop()
arb2 = foret.pop()
insert(BT(('', weight(arb1) + weight(arb2)), arb1, arb2), foret)


arb1 = foret.pop()
arb2 = foret.pop()
insert(BT(('', weight(arb1) + weight(arb2)), arb1, arb2), foret)




exple_arb_huffman = foret[0]
show_tree(exple_arb_huffman)

def huffman_tree(occ):
    '''
    :param occ: (dict) dictionnaires d'occurrences
    :return: (BinaryTree) arbre de codage de Huffman
    '''
    forest=[]
    sorted(occ.keys())
    for keys,values in occ.items():
         forest.append(BT((keys, values), VIDE , VIDE))
    while len(forest)>1:
        arb1 = forest.pop()
        arb2 = forest.pop()
        arb3 = BT(('', weight(arb1) + weight(arb2)), arb1, arb2)
        insert(arb3, forest)
    return forest[0]


#test = huffman_tree(exple_occ)
#show_tree(test)
#test.show()
#huffman_source = huffman_tree(occ_source)
#show_tree(huffman_source)
#huffman_source.show()




def tree_to_coding(tree, accu='', dico={}):
    '''
    :param tree: (BinaryTree) un arbre de Huffman
    :return: (dict) une table de codage
    '''
    if tree.is_leaf():
        symbole = tree.get_data()[0]
        dico[symbole] = accu
    if not tree.is_leaf():
        tree_to_coding(tree.get_left_subtree(), accu + '0', dico)
        tree_to_coding(tree.get_right_subtree(), accu + '1', dico)
    return dico


    

huffman_source = huffman_tree(occ_source)

codage_source = tree_to_coding(huffman_source)
#for symb in codage_source:
#    print('{:s} : {:s}'.format(symb, codage_source[symb]))



def encode(source, coding_table):
    '''
    :param source: (str) texte source à encoder
    :param coding_table: (dict) une table de codage
    :return: (str)
    '''
    code = ''
    for car in source:
        code = code + (list(coding_table.values())[list(coding_table.keys()).index(car)])
    return code


#calcul du rapport :

def bits_necessaire(texte):
    s = 0
    for car in texte:
        if ord(car) > 128:
            s = s + 16
        else:
            s = s + 8
    return s

def bits_necessaires_texte_code(texte):
    return len(texte)

#rapport de compression


#print('le rapport de compression est de :')
#print('bits_necessaire()')


##decodage du message à l'aide de la table
#
#def decode2(encoded_source, tree):
#    '''
#    :param encoded_source: (str) la chaîne binaire à décoder
#    :param tree: (BinaryTree) l'arbre de Huffman du codage utilisé
#    :return: (str) le message source décodé
#    '''
#    decod = ''
#    symb = ''
#    coding_table = tree_to_coding(tree)
#    for car in encoded_source:
#        symb = symb + str(car)
#        if symb in coding_table.keys():
#            decod = decod + coding_table[int(sym)]
#            symb = ''
#    return decod


#decodage du message à l'aide de l'arbre

def decode(encoded_source, tree):
    '''
    :param encoded_source: (str) la chaîne binaire à décoder
    :param tree: (BinaryTree) l'arbre de Huffman du codage utilisé
    :return: (str) le message source décodé
    '''
    message = ''
    arbre = tree
    for k in range(len(encoded_source)):
        bit = encoded_source[k]
        if bit == '0':
            arbre = arbre.get_left_subtree()
        elif bit == '1':
            arbre = arbre.get_right_subtree()
        else :
            print('erreur : la source à décoder n est pas en binaire')
        if arbre.is_leaf():
            message = message + arbre.get_data()[0]
            arbre = tree
    return message


occ_source = sequence_to_occurrences(texte_source)
huffman_source = huffman_tree(occ_source)
codage_source = tree_to_coding(huffman_source)
#print(len(codage_source))
texte_encode = encode(texte_source, codage_source)
texte_decode = decode(texte_encode, huffman_source)

### Rapport de compression
#print(bits_necessaires_texte_code(texte_encode)/bits_necessaire(texte_source),
#end = '\n\n')
#print(texte_encode, end='\n\n')
#print(texte_decode)