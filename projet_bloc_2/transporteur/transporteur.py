from TSP import *
from math import floor
import copy



def genere_matrice(fichier):
    '''
    fonction qui retourne la matrice des distances entre les villes d'un fichier.txt :
    nom_de_ville   lattitude    longitude  écrit
    @ param : un fichier csv ville lattitude longitude
    @ return : une matrice représentant ces données
    '''
    liste_villes = get_tour_fichier(fichier)
    nbre_villes = len(liste_villes)
    M = []
    for i in range (nbre_villes) :
        C = []
        for j in range (nbre_villes) :
            C.append(distance(liste_villes,i,j))
        M.append(C)
    return M


def nouvelle_matrice(indice, matrice):
    '''
    renvoie la matrice de laquelle on a supprimé la ième ligne et la ième colonne
    i étant l'indice choisi en argument
    @ param : matrice doit etre une matrice carrée n
              indice doit être un entier entre 0 et n-1
    @ return : une matrice carrée de dimension n-1
    '''
    matrice.pop(indice)
    for i in range(len(matrice)):
        matrice[i].pop(indice)

def ville_la_plus_proche(ville, liste_villes, matrice_des_distances):
    '''
    renvoie la ville la plus proche de "ville" présente dans liste_villes
    @ param : ville doit être dans la liste_villes et matrice_des_distances
    @ return : renvoie les données correspondant à la ville la plus proche
    effets de bord : supprime la ville de la liste_villes
                     et de matrice_des_distances
    '''
    indice_ville = liste_villes.index(ville)
    matrice_des_distances[indice_ville][indice_ville]= max(matrice_des_distances[0])+1
    indice = matrice_des_distances[indice_ville].index(min(matrice_des_distances[indice_ville]))
    ville = liste_villes[indice]
    liste_villes.pop(indice_ville)
    nouvelle_matrice(indice_ville, matrice_des_distances)
    return ville


def chemin_le_plus_court(fichier,indice_départ) :
    '''
    renvoie le chemin le plus court en partant de l'indice de départ
    et en appliquant la méthode gloutone : cherche le plus proche voisin
    @param : indice de départ doit être inférieur aux nombres de villes dans le fichier-1
    @return : renvoie la liste des villes à parcourir (avec leurs parametres lat et long)
    '''
    t = get_tour_fichier(fichier)
    m = genere_matrice(fichier)
    depart = t[indice_départ]
    nbre_villes = len(t)
    tour  = [depart]
    for _ in range(nbre_villes - 1) :
        etape = ville_la_plus_proche(depart, t, m)
        tour.append(etape)
        depart = etape
    return tour


def trouve_le_meilleur_chemin_glouton(fichier) :
    '''
    teste tous les chemins le plus court et renvoie le meilleur,c'est à dire
    celui qui a la plus petite distance totale
    '''
    f = get_tour_fichier(fichier)
    nbre_villes = len (f)
    sol = 0
    ref = longueur_tour(chemin_le_plus_court(fichier,0))
    for i in range(nbre_villes-1):
        test = longueur_tour(chemin_le_plus_court(fichier,i+1))
        if test < ref :
            ref = test
            sol = i+1
    return chemin_le_plus_court(fichier,sol)


#on cherche le chemin le plus court avec la méthode gloutonne en partant
# de la première ville du fichier

best = chemin_le_plus_court('exemple.txt',0)

#on trace la représentation graphique de ce chemin

trace(best)



#on cherche puis on affiche le meilleur chemin obtenu par la méthode gloutonne

best_of_the_best = trouve_le_meilleur_chemin_glouton('exemple.txt')

#on affiche la représentation graphique de ce meilleur chemin
trace(best_of_the_best)






