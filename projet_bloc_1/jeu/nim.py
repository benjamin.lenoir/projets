from random import randint

def situation_initiale():
    '''
    Mise en place du jeu selon les règles
    '''
    try:
        print()
        nb_cailloux = int(input('Avec combien de cailloux, voulez-vous jouer ? '))
        assert 2 <= nb_cailloux
        print('')
        print('Il y a {} cailloux'.format(nb_cailloux))
        c =''
        for _ in range(nb_cailloux):
            c = c+'O '
        print(c)
        return nb_cailloux

    except ValueError:
        print('Veuillez saisir un nombre')
        return situation_initiale()
        
    except AssertionError:
        print('Je pense qu\'il faut plus de cailloux ^^')
        return situation_initiale()



def situation_courante(nb_cailloux):
    '''
    Donne la situation du jeu/plateau à un instant t
    @ param nb_cailloux: (int) nombre de cailloux restants
    '''
    print('')
    print('Il reste {} cailloux'.format(nb_cailloux))
    c =''
    for _ in range(nb_cailloux):
        c = c+'O '
    print(c)



def game_over(nb_cailloux,joueur,nom_des_joueurs):
    '''
    Détermine la fin d'une partie avec affichage du résultat
    @ param nb_cailloux: (int) nombre de cailloux restants (0 ou 1)
    @ param joueur: (str) type de joueur (humain ou machine)
    Exemple:
    >>> nom_des_joueurs = ['Tic' , 'Tac']
    >>> game_over(0,'humain',nom_des_joueurs)
    'Le joueur Tic a perdu'
    '''
    if joueur == 'humain':
        joueur = nom_des_joueurs[0]
    else :
        joueur = nom_des_joueurs[1]
    if nb_cailloux == 0:
        print('Le joueur {} a perdu'.format(joueur))
    elif nb_cailloux == 1:
        print('Le joueur {} a gagné'.format(joueur))
    else:
        print('Les programmeurs ont merdé quelque part... ^^')



def choix_humain(nb_cailloux,nom_des_joueurs):
    '''
    Action du joueur humain selon les possibilités
    @ param nb_cailloux: (int) nombre de cailloux restants
    @ param nom_des_joueurs: (str) nom du joueur
    '''
    try:
        print('{} , combien prenez-vous de cailloux ?'.format(nom_des_joueurs[0]))
        res = int(input())
        print('')
        assert 1 < res < 4
        if nb_cailloux < 3:
            try:
                assert res == 2
            except AssertionError:
                print('Le nombre de cailloux choisi ne peut être que de 2. ')
                return choix_humain(nb_cailloux,nom_des_joueurs)
    
    except AssertionError:
        print('Le nombre de cailloux choisi n\'est pas correct, il faut en choisir 2 ou 3. ')
        return choix_humain(nb_cailloux,nom_des_joueurs)
    
    except ValueError:
        print('Veuillez saisir un nombre')
        return choix_humain(nb_cailloux,nom_des_joueurs)
    
    return nb_cailloux-res



def test_poursuite(nb_cailloux):
    '''
    Vérifie si la partie peut continuer ou si elle est finie
    @ param nb_cailloux: (int) nombre de cailloux restants (0 ou 1)
    @ return: True si la partie peut continuer, False sinon
    Exemple:
    >>> test_poursuite(20)
    True
    >>> test_poursuite(1)
    False
    '''
    
    if nb_cailloux >= 2:
        return True
    else:
        return False


#### Différentes difficultés d'adversaire

def choix_machine_1(nb_cailloux,nom_des_joueurs):
    '''
    Action du joueur machine (ici humain en fait)
    @ param nb_cailloux: (int) nombre de cailloux restants
    @ param nom_des_joueurs: (str) nom du joueur
    return: (int) nombre de cailloux restants
    '''
    try:
        print('{} , combien prenez-vous de cailloux ?'.format(nom_des_joueurs[1]))
        res = int(input())
        print('')
        assert 1 < res < 4
        
    except AssertionError:
        print('Le nombre de cailloux choisi n\'est pas correct, il faut en choisir 2 ou 3. ')
        return choix_humain(nb_cailloux,nom_des_joueurs)
    
    except ValueError:
        print('Veuillez saisir un nombre')
        return choix_humain(nb_cailloux,nom_des_joueurs)
    
    return nb_cailloux-res



def choix_machine_2(nb_cailloux,nom_des_joueurs):
    '''
    Action du joueur machine (choix aléatoire)
    @ param nb_cailloux: (int) nombre de cailloux restants
    @ param nom_des_joueurs: (str) nom du joueur
    return: (int) nombre de cailloux restants
    Exemple:
    >>> nom_des_joueurs = ['Tic' , 'Tac']
    >>> choix_machine_2(20,nom_des_joueurs)
    17
    '''
    
    return nb_cailloux-randint(2,3)

    
    
def choix_machine_3(situation,nom_des_joueurs):
    '''
    Action du joueur machine (minmax)
    @ param nb_cailloux: (int) nombre de cailloux restants
    @ param nom_des_joueurs: (str) nom du joueur
    return: (int) nombre de cailloux restants
    '''
    situation = min_max(situation, 3, nom_des_joueurs[1])
    

### Partie minmax

def test_finalite(nb_cailloux):
    '''
    Vérifie si la partie peut continuer ou si elle est finie
    @ param nb_cailloux: (int) nombre de cailloux restants (0 ou 1)
    @ return: True si la partie peut continuer, False sinon
    Exemple:
    >>> test_finalite(3)
    False
    >>> test_finalite(2)
    True
    '''
    
    if nb_cailloux < 3:
        return True
    else:
        return False


def evaluation(situation):
    '''
    Donne une note à la situation suivant le joueur
    @ param situation: (int) nombre de cailloux restants (0 ou 1)
    return: (int) 1, 0 ou -1 selon la situation
    Exemple:
    >>> evaluation(6)
    0
    >>> evaluation(3)
    1
    >>> evaluation(1)
    -1
    '''
    if situation in [3,4,5] :
        return 1
    elif situation in [0,1,2] :
        return -1
    else :
        return 0

    
def situ_suiv(situation):
    '''
    Renvoie une liste de toutes les situations suivantes à celle rentrée en paramètre
    return: (list) liste de toutes les situations suivantes
    Exemple:
    >>> situ_suiv(6)
    [4, 3]
    '''
    return [situation-2,situation-3]

