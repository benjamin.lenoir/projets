#import nim as jeu
#import tictactoe as jeu

DIFFICULTE = 3 #on pourra la demander par la suite pour augmenter le niveau de difficulté

def changement_joueur(joueur):
    '''
    Permet l'alternance du joueur
    @ param joueur: (str) type de joueur (humain ou machine)
    return: change le joueur et renvoie humain si erreur d'argument
    
    Exemple:
    >>> changement_joueur('humain')
    'machine'
    '''
    
    if joueur == 'humain':
        joueur = 'machine'
    else:
        joueur = 'humain'
    return joueur



def nom_machine(adversaire):
    '''
    Permet de déterminer les noms à donner selon le niveau de difficulté choisi
    @ param adversaire: (int) niveau de difficulté
    return: (list) nom des joueurs 
    '''

    if adversaire == 1:
        nom_des_joueurs = [input('Quel est votre pseudo ? '),input('Quel est le pseudo du 2ème joueur ? ')]
    elif adversaire == 2:
        nom_des_joueurs = [input('Quel est votre pseudo ? '),'La gentille machine']
    else:    
        nom_des_joueurs = [input('Quel est votre pseudo ? '),'La méchante machine']
    
    return nom_des_joueurs


def coeff(joueur):
    '''
    Renvoie un coefficient
    return : renvoie 1 si il s'agit du joueur actif et -1 sinon
    Exemple:
    >>> coeff('humain')
    -1
    >>> coeff('machine')
    1
    '''
    if joueur == 'machine' :
        return 1
    else :
        return -1


def min_max(situation, profondeur, joueur):
    '''
    Algorithme classique min_max
    @ param : situation est la situation à évaluer par l algo min_max
    @ param : profondeur est le nombre de coups à l avance regardés
    @ param : joueur est le joueur actif dont on évalue la situation
    return : renvoie le score optimal obtenu pour le joueur avec la situation donnée et la profondeur choisie ainsi que la configuration
                permettant de l'obtenir
    '''
    if jeu.test_finalite(situation): 
        return jeu.evaluation(situation)*coeff(joueur),jeu.situ_suiv(situation,joueur)[0]
    elif profondeur == 0 :
        return 0,jeu.situ_suiv(situation,joueur)[0]
    else :
        situations_suivantes = jeu.situ_suiv(situation,joueur)
        if joueur == 'machine' :
            L = [ min_max(situ, profondeur-1, 'humain')[0] for situ in situations_suivantes ]
            score = max(L)
        else :
            L = [ min_max(situ, profondeur-1, 'machine')[0] for situ in situations_suivantes ] 
            score = min(L)
        situ_optimale = situations_suivantes[L.index(score)]  
        return score,situ_optimale


def jouons(adversaire):
    '''
    Lance le jeu importé
    @ param adversaire: (int) niveau de difficulté
    '''
    print('Que la partie commence !\n \n')
    joueur = 'humain'
    nom_des_joueurs = nom_machine(adversaire)
    situation = jeu.situation_initiale()
    situation = jeu.choix_humain(situation , nom_des_joueurs)
    jeu.situation_courante(situation)
    
    while jeu.test_poursuite(situation):
        joueur = changement_joueur(joueur)
        
        if joueur == 'humain':
            situation = jeu.choix_humain(situation,nom_des_joueurs)
        else:
            if adversaire == 1:
                situation = jeu.choix_machine_1(situation,nom_des_joueurs)
            else:
                print('')
                print('{} a joué'.format(nom_des_joueurs[1]))
                if adversaire == 2:
                    situation = jeu.choix_machine_2(situation,nom_des_joueurs) ### Choix machine aléatoire
                else:
                    situation = min_max(situation,DIFFICULTE,nom_des_joueurs[1])[1] ### Choix machine minmax
        
        jeu.situation_courante(situation)
    
    jeu.game_over(situation,joueur,nom_des_joueurs)
        
    
if __name__ == '__main__':

### Menu pour l'import des modules voulus et le choix du niveau de difficulté

    
    print()
    print("Jeux proposés :")
    print(" 1 --> Jeu de Nim")
    print(" 2 --> Tic-Tac-Toe")
    print()
    choix = int(input("Choix du jeu: "))        
    if choix == 1:
        import nim as jeu
    elif choix == 2:
        import tictactoe as jeu
    
    print()
    print("Adversaires :")
    print(" 1 --> Humain contre humain")
    print(" 2 --> Humain contre une machine gentille")
    print(" 3 --> Humain contre une méchante machine")
    print()
    adversaire = int(input("Choix de l'adversaire: "))

    jouons(adversaire)


### Import dynamique des modules voulus

#    import sys
#    if len(sys.argv) < 2:
#        module = 'nim'
#    elif:
#        module = sys.argv[1]
#    jeu = __import__(module)

#    jouons()