def situation_initiale():
    '''
    Mise en place du jeu selon les règles
    '''
    nb_cailloux = int(input('Avec combien de cailloux, voulez-vous jouer ? '))
    print('')
    print('Il y a {} cailloux'.format(nb_cailloux))
    c =''
    for _ in range(nb_cailloux):
        c = c+'O '
    print(c)
    return nb_cailloux


def situation_courante(nb_cailloux):
    '''
    Donne la situation du jeu/plateau à un instant t
    @ param nb_cailloux: (int) nombre de cailloux restants
    '''
    print('Il reste {} cailloux'.format(nb_cailloux))
    c =''
    for _ in range(nb_cailloux):
        c = c+'O '
    print(c)


def game_over(nb_cailloux,joueur):
    '''
    Détermine la fin d'une partie avec affichage du résultat
    @ param nb_cailloux: (int) nombre de cailloux restants (0 ou 1)
    @ param joueur: (str) type de joueur (humain ou machine)
    '''
    
    if nb_cailloux == 0:
        print('\n')
        print('Le joueur {} a perdu'.format(joueur))
    elif nb_cailloux == 1:
        print('\n')
        print('Le joueur {} a gagné'.format(joueur))
    else:
        print('\n')
        print('\n')
        print('\n')
        print('Bien tenté mais il ne restait que 2 cailloux. Le joueur{} a perdu'.format(joueur))



    
def choix_humain(nb_cailloux):
    '''
    Action du joueur humain selon les possibilités
    @ param nb_cailloux: (int) nombre de cailloux restants
    '''
    try:
        print('Combien prenez-vous de cailloux ?'.format(nb_cailloux))
        res = int(input())
        print('')
        assert 1 < res < 4
        
    except AssertionError:
        print('Le nombre de cailloux choisi n\'est pas correct, il faut en choisir 2 ou 3. ')
        return choix_humain(nb_cailloux)
    
    except ValueError:
        print('Veuillez saisir un nombre')
        return choix_humain(nb_cailloux)
    
    return nb_cailloux-res



def choix_machine(nb_cailloux):
    '''
    Action du joueur machine selon l'algorithme voulu (ici min_max)
    '''
    if nb_cailloux % 5 == 4:
        res = 3
    else:
        res = 2
    print('Le joueur machine a retiré {} cailloux'.format(res))
    print('')
    return nb_cailloux-res



def test_poursuite(nb_cailloux):
    '''
    Vérifie si la partie peut continuer ou si elle est finie
    @ param nb_cailloux: (int) nombre de cailloux restants (0 ou 1)
    @ return: True si la partie peut continuer, False sinon
    '''
    
    if nb_cailloux >= 2:
        return True
    else:
        return False
    
    
def test_finalite(situation):
    '''
    Vérifie si la partie peut continuer ou si elle est finie
    @ param nb_cailloux: (int) nombre de cailloux restants (0 ou 1)
    @ return: True si la partie peut continuer, False sinon
    '''
    
    if situation < 3:
        return True
    else:
        return False


def evaluation(situation):
    '''
    donne une note à la situation suivant le joueur
    '''
    if situation in [3,4,5] :
        return -1
    elif situation in [0,1,2] :
        return 1
    else :
        return 0

    
def situ_suiv(situation, joueur):
    '''
    @return : renvoie une liste de toutes les situations suivantes  à celle rentrée en paramètre
    '''
    return [situation-2,situation-3]
    
    

